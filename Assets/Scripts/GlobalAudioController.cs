﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GlobalAudioController : MonoBehaviour
{
    public static GlobalAudioController Instance { get; private set; }

    private void Awake() {
        if (Instance != null) {
            Debug.Log("we only want one player config manager");
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(Instance); //persists game objects across scenes
        }
    }
}
