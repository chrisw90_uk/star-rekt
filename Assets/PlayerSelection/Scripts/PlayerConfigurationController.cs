﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerConfigurationController : MonoBehaviour
{
    private List<PlayerConfiguration> playerConfigs;
    public int minPlayers = 1;

    public static PlayerConfigurationController Instance { get; private set; }

    private void Awake() {
        if (Instance != null) {
            Debug.Log("we only want one player config manager");
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(Instance); //persists game objects across scenes
            playerConfigs = new List<PlayerConfiguration>();
        }
    }

    public List<PlayerConfiguration> GetPlayerConfigs() {
        return playerConfigs;
    }

    public void SetPlayerSprite(int i, Sprite sprite) {
        playerConfigs[i].playerSprite = sprite;
    }

    public void SetAbilities(int i, List<GameObject> abilitySlots) {
        List<Ability> abilities = new List<Ability>();
        for (int index = 0; index < abilitySlots.Count; index++)
        {
            GameObject item = abilitySlots[index];
            abilities.Add(new Ability(item.GetComponent<AbilitySlotController>().key, item.GetComponent<AbilitySlotController>().amount));
        }
        playerConfigs[i].abilities = abilities;
    }
    public void SetPlayerReady(int i) {
        playerConfigs[i].isReady = true;
        if(playerConfigs.Count >= minPlayers && playerConfigs.All(p => p.isReady == true)) {
            foreach (var p in playerConfigs)
            {
                foreach (Ability a in p.abilities) {
                    p.ApplyAbility(a.key, a.value);
                }
            }
            SceneManager.LoadScene("Game");
        }
    }

    public void SetPlayerSpawn(int i, GameObject spawn) {
        playerConfigs[i].playerSpawn = spawn;
    }
    public void SetPlayerHUD(int i, GameObject hud) {
        playerConfigs[i].playerHud = hud;
    }

    public void HandlePlayerJoin(PlayerInput pi) {
        Debug.Log("Player Joined: " + pi.playerIndex);
        if (!playerConfigs.Any(p => p.playerIndex == pi.playerIndex)) {
            pi.transform.SetParent(transform);
            playerConfigs.Add(new PlayerConfiguration(pi));
        }
    }
}

public class PlayerConfiguration
{
    public GameObject playerHud { get; set; }
    public GameObject playerSpawn { get; set; }
    public PlayerInput playerInput { get; set; }
    public GameObject playerPanel { get; set; }
    public int playerIndex { get; set; }
    public bool isReady { get; set; }
    public Sprite playerSprite { get; set; }
    public float kills { get; set; }
    public float deaths { get; set; }
    public float kdr { get; set; }
    public float shotsFired { get; set; }
    public float shotsOnTarget { get; set; }
    public int damageDealt { get; set; }
    public int damageTaken { get; set; }
    public int criticalHits { get; set; }
    public int criticalHitDamage { get; set; }
    public float accuracy { get; set; }

    // Abilities

    public List<Ability> abilities { get; set; }

    // Weapon charges
    public int additionalDamage { get; set; }
    public int primaryCharges { get; set; }
    public float primaryCooldown { get; set; }
    public int secondaryCharges { get; set; }
    public float secondaryCooldown { get; set; }

    // Health and shields
    public float health { get; set; }
    public float shieldStrength { get; set; }

    // Critical hit
    public float critHitChance { get; set; }

    // Boost
    public float boost { get; set; }

    public PlayerConfiguration(PlayerInput pi) {
        playerIndex = pi.playerIndex;
        playerInput = pi;

        // Set default game values
        primaryCharges = 8;
        primaryCooldown = 1;
        secondaryCharges = 3;
        secondaryCooldown = 5;
        health = 100;
        shieldStrength = 100;
        critHitChance = 0.2f;
        additionalDamage = 0;
        boost = 100;

        //Set default stats
        kills = 0;
        deaths = 0;
        kdr = 0;
        shotsFired = 0;
        shotsOnTarget = 0;
        damageDealt = 0;
        damageTaken = 0;
        criticalHits = 0;
        criticalHitDamage = 0;
        accuracy = 0;
    }

    // Abilities
    public void AddPrimaryCharges (int charges) {
        primaryCharges += charges;
    }
    public void AddSecondaryCharges (int charges) {
        secondaryCharges += charges;
    }
    public void ReducePrimaryCooldown(float percent) {
        primaryCooldown = primaryCooldown * percent;
    }
    public void ReduceSecondaryCooldown(float percent) {
        secondaryCooldown = secondaryCooldown * percent;
    }
    public void IncreaseHealth(float percent) {
        health = health * percent;
    }
    public void IncreaseShield(float percent) {
        shieldStrength = shieldStrength * percent;
    }
    public void IncreaseBoost(float percent) {
        boost = boost * percent;
    }
    public void IncreaseCritHitChance(float percent) {
        critHitChance += percent;
    }
    public void AddAdditionalDamage(int value) {
        additionalDamage += value;
    }

    public void ApplyAbility(string key, string value) {
        switch (key)
        {
            case "health":
                IncreaseHealth(float.Parse(value));
                break;
            case "boost":
                IncreaseBoost(float.Parse(value));
                break;
            case "shield":
                IncreaseShield(float.Parse(value));
                break;
            case "primary_ammo":
                AddPrimaryCharges(int.Parse(value));
                break;
            case "cooldown":
                ReducePrimaryCooldown(float.Parse(value));
                ReduceSecondaryCooldown(float.Parse(value));
                break;
            case "secondary_ammo":
                AddSecondaryCharges(int.Parse(value));
                break;
            case "crit_hit":
                IncreaseCritHitChance(float.Parse(value));
                break;
            case "damage":
                AddAdditionalDamage(int.Parse(value));
                break;
            default:
                break;
        }
    }

    // Stats and scores
    public void AddKill() {
        kills++;
        SetKDR();
    }
    public void AddDeath() {
        deaths++;
        SetKDR();
    }
    private void SetKDR() {
        kdr = deaths > 0 ? kills / deaths : kills;
    }
    public void AddShotFired() {
        shotsFired++;
        SetAccuracy();
    }
    public void AddShotOnTarget(int damage, bool isCriticalHit) {
        shotsOnTarget++;
        damageDealt += damage;
        SetAccuracy();
        if (isCriticalHit) {
            criticalHits++;
            criticalHitDamage += damage;
        }
    }
    public void TakeDamage(int damage) {
        damageTaken += damage;
    }
    private void SetAccuracy() {
        accuracy = (shotsOnTarget / shotsFired) * 100;
    }
}
