﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class Ability
{
    public string key { get; set; }
    public string value { get; set; }
    public Ability(string abilityKey, string abilityValue) {
        key = abilityKey;
        value = abilityValue;
    }
}
