﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;

public class PlayerJoinController : MonoBehaviour
{
    public GameObject panel;
    public PlayerInput input;
    
    private void Awake() {
        var menu = GameObject.Find("PlayerSetupCanvas");
        if (menu != null) {
            input = GetComponent<PlayerInput>();
            int i = input.playerIndex;
            panel = menu.GetComponent<PlayerSetupCanvasController>().playerPanels[i];
            input.uiInputModule = panel.GetComponentInChildren<InputSystemUIInputModule>();
            panel.GetComponent<PlayerSetupPanelController>().SetPlayerIndex(i);
            panel.GetComponent<PlayerSetupPanelController>().EnableSetupPanel();
            panel.GetComponent<PlayerSetupPanelController>().DisableJoinPanel();
        }
    }
}
