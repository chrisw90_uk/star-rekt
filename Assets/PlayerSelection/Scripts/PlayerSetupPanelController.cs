﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class PlayerSetupPanelController : MonoBehaviour
{
    private int playerIndex;
    public TextMeshProUGUI playerName;
    private float ignoreInputTime = 0.5f;
    private bool inputEnabled;
    public Button readyButton;
    public GameObject joinPanel;
    public GameObject setupPanel;
    public EventSystem eventSystem;

    // Sprite selection
    public Sprite[] spriteOptions;
    public GameObject spriteSelection;
    private int currentSpriteIndex = 0;

    // Ability selection
    public GameObject activeAbilitySlot;
    public GameObject firstAbility;
    public List<GameObject> abilitySlots;

    void Update()
    {
        if (setupPanel.activeSelf && (Time.time > ignoreInputTime)) {
            inputEnabled = true;
        }
    }

    // Enable/disable

    public void EnableSetupPanel() {
        setupPanel.gameObject.SetActive(true);
        PlayerConfigurationController.Instance.SetPlayerSprite(playerIndex, spriteOptions[0]);
    }
    public void DisableJoinPanel() {
        joinPanel.gameObject.SetActive(false);
    }
    public void SetPlayerIndex(int i) {
        playerIndex = i;
        playerName.SetText("Player " + (i + 1).ToString());
        ignoreInputTime = Time.time + ignoreInputTime;
    }

    // Sprite selection

    public void PrevSprite() {
        if (!inputEnabled) { return; }
        if (currentSpriteIndex == 0) {
            currentSpriteIndex = spriteOptions.Length - 1;
        } else {
            currentSpriteIndex--;
        }
        SetSprite(spriteOptions[currentSpriteIndex]);
    }
    public void NextSprite () {
        if (!inputEnabled) { return; }
        if (currentSpriteIndex + 1 == spriteOptions.Length) {
            currentSpriteIndex = 0;
        } else {
            currentSpriteIndex++;
        }
        SetSprite(spriteOptions[currentSpriteIndex]);
    }
    public void SetSprite(Sprite image) {
        spriteSelection.GetComponent<Image>().sprite = image;
        PlayerConfigurationController.Instance.SetPlayerSprite(playerIndex, image);
    }

    // Ability selection

    public void FocusAbilityPanel() {
        activeAbilitySlot = EventSystem.current.currentSelectedGameObject;
        EventSystem.current.SetSelectedGameObject(firstAbility);
    }
    public void CancelAbilityPanel() {
        EventSystem.current.SetSelectedGameObject(activeAbilitySlot);
        activeAbilitySlot = null;
    }
    public void SelectAbility() {
        GameObject button = EventSystem.current.currentSelectedGameObject;
        TextMeshProUGUI abilityButtonText = activeAbilitySlot.GetComponentInChildren<TextMeshProUGUI>();
        abilityButtonText.text = button.GetComponentInChildren<TextMesh>().text;
        activeAbilitySlot.GetComponent<AbilitySlotController>().key = button.GetComponent<AbilityButtonController>().key;
        activeAbilitySlot.GetComponent<AbilitySlotController>().amount = button.GetComponent<AbilityButtonController>().amount;
        button.GetComponent<Button>().interactable = false;
        CancelAbilityPanel();
    }

    // Ready

    public void ReadyPlayer() {
        if (!inputEnabled) { return; }
        PlayerConfigurationController.Instance.SetAbilities(playerIndex, abilitySlots);
        PlayerConfigurationController.Instance.SetPlayerReady(playerIndex);
        readyButton.gameObject.SetActive(false);
    }
}
