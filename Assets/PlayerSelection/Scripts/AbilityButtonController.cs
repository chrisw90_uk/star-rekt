﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class AbilityButtonController : MonoBehaviour, ICancelHandler {

    public GameObject Text;
    public string key;
    public string amount;

    void ICancelHandler.OnCancel(BaseEventData eventData){
        GetComponentInParent<PlayerSetupPanelController>().CancelAbilityPanel();
    }
    public void OnHoverButton() {
        StartCoroutine(AnimateIconTo(1.1f, 0.3f));
        StartCoroutine(FadeTextTo(1f));
    }
    public void OnLeaveButton () {
        StartCoroutine(AnimateIconTo(1f, 1f));
        StartCoroutine(FadeTextTo(0f));
    }

    IEnumerator AnimateIconTo(float scale, float opacity) {
        for (float t = 0.0f; t < 1f; t += Time.deltaTime / 0.4f) {
            float currentScale = transform.localScale.x;
            Color currentColor = GetComponent<Image>().color;
            GetComponent<Image>().color = new Color(currentColor.r, currentColor.g, currentColor.b, Mathf.Lerp(currentColor.a, opacity, t));
            transform.localScale = new Vector2(Mathf.Lerp(currentScale, scale, t), Mathf.Lerp(currentScale, scale, t));
            yield return null;
        }
    }

    IEnumerator FadeTextTo(float opacity) {
        for (float t = 0.0f; t < 1f; t += Time.deltaTime / 0.4f) {
            Color currentColor = Text.GetComponent<TextMesh>().color;
            Text.GetComponent<TextMesh>().color = new Color(currentColor.r, currentColor.g, currentColor.b, Mathf.Lerp(currentColor.a, opacity, t));
            yield return null;
        }
    }
}