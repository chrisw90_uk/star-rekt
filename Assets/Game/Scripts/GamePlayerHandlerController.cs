using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GamePlayerHandlerController : MonoBehaviour
{
    // Timer

    public float timeRemaining = 30;
    public bool timerIsRunning = false;
    public GameObject timer;
    
    // Players
    
    private List<PlayerConfiguration> playerConfigs;
    public GameObject playerObject;
    public GameObject[] playerSpawns;

    // Game End

    public GameObject gameEndScreen;

    private void Start() {
        timerIsRunning = true;

        playerConfigs = PlayerConfigurationController.Instance.GetPlayerConfigs();
        GameObject gameHUD = GameObject.Find("GameHUDCanvas");
        foreach (var p in playerConfigs)
        {
            // Spawn player
            var player = Instantiate(playerObject, playerSpawns[p.playerIndex].transform.position, playerSpawns[p.playerIndex].transform.rotation, transform);
            // Store spawn point
            PlayerConfigurationController.Instance.SetPlayerSpawn(p.playerIndex, playerSpawns[p.playerIndex]);
            GameObject playerHud = gameHUD.GetComponent<GameHUDController>().CreateHUDForPlayer(p);
            playerHud.GetComponent<PlayerHUDController>().playerObject = player;
            PlayerConfigurationController.Instance.SetPlayerHUD(p.playerIndex, playerHud);
            player.GetComponent<PlayerController>().Init(playerConfigs[p.playerIndex]);
            player.GetComponent<PlayerWeaponController>().Init(playerConfigs[p.playerIndex]);
        }
    }

    void Update()
    {
        if (timerIsRunning) {
            if (timeRemaining > 0) {
                timeRemaining -= Time.deltaTime;
            } else {
                timeRemaining = 0;
                timerIsRunning = false;
                EndGame();
            }
            FormatTimeToDisplay(timeRemaining);
        }
    }

    private void EndGame() {
        FormatTimeToDisplay(0);
        gameEndScreen.SetActive(true);
        gameEndScreen.GetComponent<GameEndController>().SetScores(playerConfigs);
        foreach (var p in playerConfigs)
        {
            DisablePlayerInput(p);
        }
    }

    private void FormatTimeToDisplay(float time) {
        float minutes = Mathf.FloorToInt(time / 60);  
        float seconds = Mathf.FloorToInt(time % 60);
        timer.GetComponent<TextMeshProUGUI>().text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    private void DisablePlayerInput(PlayerConfiguration player) {
        player.playerInput.DeactivateInput();
    }
}
