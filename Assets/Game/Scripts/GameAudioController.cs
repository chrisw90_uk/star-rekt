﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameAudioController : MonoBehaviour
{
    public static GameAudioController Instance { get; private set; }
    AudioSource audio;
    public AudioClip primaryWeaponFire;
    public AudioClip primaryWeaponShieldImpact;
    private void Awake() {
        if (Instance != null) {
            Debug.Log("we only want one game audio controller");
        }
        else
        {
            Instance = this;
        }
    }

    private void Start() {
        audio = GetComponent<AudioSource>();
    }
    public void PlayPrimaryWeaponFire() {
        audio.PlayOneShot(primaryWeaponFire);
    }
    public void PlayPrimaryWeaponShieldImpact() {
        audio.PlayOneShot(primaryWeaponShieldImpact);
    }
}
