using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using TMPro;

public class PlayerHUDController : MonoBehaviour
{
    public int playerId;
    public PlayerConfiguration playerConfig { get; set; }
    public GameObject playerObject { get; set; }
    public Image healthMeter;
    public GameObject killStat;
    public GameObject deathStat;
    public GameObject primaryCharge;
    public GameObject secondaryCharge;
    public float playerKills = 0f;
    public float playerDeaths = 0f;
    public float playerKDR = 0f;
    public float playerHealth = 3f;

    // Boost
    public Image boostMeter;

    // Primary Cooldown
    public float primaryCooldown;
    private float currentPrimaryCooldown;
    public bool isOnPrimaryCooldown { get; set; }
    public Image primaryCooldownMeter;

    // Secondary Cooldown
    public float secondaryCooldown;
    private float currentSecondaryCooldown;
    public bool isOnSecondaryCooldown { get; set; }
    public Image secondaryCooldownMeter;

    private void Update() {
        if (isOnPrimaryCooldown) {
            ManagePrimaryWeaponCooldown();
        }
        if (isOnSecondaryCooldown) {
            ManageSecondaryWeaponCooldown();
        }
    }
    private void ManagePrimaryWeaponCooldown() {
        if (currentPrimaryCooldown > 0) {
            currentPrimaryCooldown -= Time.deltaTime;
            primaryCooldownMeter.fillAmount = currentPrimaryCooldown / primaryCooldown;
        } else {
            currentPrimaryCooldown = primaryCooldown;
            primaryCooldownMeter.fillAmount = 0;
            playerObject.GetComponent<PlayerWeaponController>().AddPrimaryCharge();
        }
    }

    private void ManageSecondaryWeaponCooldown() {
        if (currentSecondaryCooldown > 0) {
            currentSecondaryCooldown -= Time.deltaTime;
            secondaryCooldownMeter.fillAmount = currentSecondaryCooldown / secondaryCooldown;
        } else {
            currentSecondaryCooldown = secondaryCooldown;
            secondaryCooldownMeter.fillAmount = 0;
            playerObject.GetComponent<PlayerWeaponController>().AddSecondaryCharge();
        }
    }

    public void SetPrimaryCooldown (float value) {
        primaryCooldown = value;
        currentPrimaryCooldown = value;
    }
    public void SetSecondaryCooldown (float value) {
        secondaryCooldown = value;
        currentSecondaryCooldown = value;
    }
    public void StartPrimaryCooldown() {
        isOnPrimaryCooldown = true;
        currentPrimaryCooldown = primaryCooldown;
    }

    public void StartSecondaryCooldown() {
        isOnSecondaryCooldown = true;
        currentSecondaryCooldown = secondaryCooldown;
    }

    public void Init(PlayerConfiguration p) {
        playerConfig = p;
        UpdateId(p.playerIndex);
        Spawn();
    }
    public void UpdateId (int id) {
        playerId = id;
    }
    public void SetBoostAmount(float amount) {
        boostMeter.fillAmount = amount / playerConfig.boost;
    }
    public void TakeDamage(float damage) {
        playerHealth -= damage;
        healthMeter.fillAmount = playerHealth / playerConfig.health;
    }
    public void AddKill() {
        playerKills += 1;
        killStat.GetComponent<TextMesh>().text = playerKills.ToString();
    }
    public void AddDeath() {
        playerDeaths += 1;
        deathStat.GetComponent<TextMesh>().text = playerDeaths.ToString();
    }
    public void SetPrimaryCharge(int charge) {
        primaryCharge.GetComponent<TextMesh>().text = charge.ToString();
    }
    public void SetSecondaryCharge(int charge) {
        secondaryCharge.GetComponent<TextMesh>().text = charge.ToString();
    }
    public void Spawn() {
        playerHealth = playerConfig.health;
        healthMeter.fillAmount = 1;
        boostMeter.fillAmount = 1;
        var quadrants = GetComponentsInChildren<PlayerHUDShieldQuadrantController>(true);
        //Loop through quadrants and restore health
        foreach (var quadrant in quadrants)
            quadrant.RestoreHealth();
    }

    // Shields
    public void TakeShieldDamage(string quadrantName, int damage) {
        var shields = transform.Find("PlayerHUDShields").gameObject;
        var shieldQuadrantHit = shields.transform.Find(quadrantName);
        shieldQuadrantHit.GetComponent<PlayerHUDShieldQuadrantController>().TakeDamage(damage);
    }

    public void DestroyShield(string quadrantName) {
        var shields = transform.Find("PlayerHUDShields").gameObject;
        var shieldQuadrantHit = shields.transform.Find(quadrantName);
        shieldQuadrantHit.GetComponent<PlayerHUDShieldQuadrantController>().Destroy();
    }
}
