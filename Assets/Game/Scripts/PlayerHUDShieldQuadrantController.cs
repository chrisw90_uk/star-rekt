﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHUDShieldQuadrantController : MonoBehaviour
{
    private int health = 0;
    private int currentHealth;
    public Image shieldFill;

    public void TakeDamage(int damage) {
        currentHealth -= damage;
        float fillAmount = (float)currentHealth / (float)health;
        shieldFill.fillAmount = fillAmount;
        if (currentHealth <= 0) {
            gameObject.SetActive(false);
        }
    }

    public void Destroy() {
        currentHealth = 0;
        gameObject.SetActive(false);
    }

    public void RestoreHealth(int value = 0) {
        if (health == 0 && value != 0) {
            health = value;
        }
        if(!gameObject.activeSelf){
            gameObject.SetActive(true);
        }
        currentHealth = health;
        shieldFill.fillAmount = 1;
    }
}
