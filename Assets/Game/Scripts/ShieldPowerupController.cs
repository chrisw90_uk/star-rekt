﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldPowerupController : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D col) {
    	if(col.tag == "Player"){
			var quadrants = col.gameObject.GetComponentsInChildren<ShieldQuadrantController>(true);
			//Loop through quadrants and restore health
			foreach (var quadrant in quadrants)
            	quadrant.RestoreHealth();
        	Destroy(gameObject);
    	}
    }
}
