using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrimaryWeaponController : MonoBehaviour
{

    public float speed;
    private int minDamage = 15;
    private int maxDamage = 30;
    public LayerMask canHit;
    public PlayerConfiguration originatedFrom;
    public GameObject primaryWeaponHit;
    public float criticalHitChance;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroyBeam", 3f);
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, transform.up, 0.1f, canHit);
        if (hitInfo.collider != null) {
            bool isPlayerShield = hitInfo.collider.CompareTag("Shield");
            bool isPlayerHull = hitInfo.collider.CompareTag("Player");
            var playerIndex = -1;
            if (isPlayerShield || isPlayerHull) {
                playerIndex = hitInfo.collider.transform.GetComponentInParent<PlayerController>().playerConfig.playerInput.playerIndex;
            }
            if (playerIndex != originatedFrom.playerIndex) {
                int damage = Random.Range(minDamage, maxDamage);
                damage += originatedFrom.additionalDamage;
                bool criticalHit = false;
                if (Random.Range(0f, 1f) < criticalHitChance) {
                    damage += 8;
                    criticalHit = true;
                }
                if (isPlayerShield) {
                    GameAudioController.Instance.PlayPrimaryWeaponShieldImpact();
                    var target = hitInfo.collider.transform.parent.parent;
                    PlayerConfiguration targetConfig = target.GetComponent<PlayerController>().playerConfig;
                    targetConfig.TakeDamage(damage);
                    originatedFrom.AddShotOnTarget(damage, false);
                    hitInfo.collider.GetComponent<ShieldQuadrantController>().TakeDamage(damage);
                    Debug.Log(hitInfo.collider.name);
                    targetConfig.playerHud.GetComponent<PlayerHUDController>().TakeShieldDamage(hitInfo.collider.name, damage);
                } else if (isPlayerHull) {
                    var target = hitInfo.collider.transform.parent;
                    if (target.GetComponent<PlayerHealthController>() != null) {
                        PlayerConfiguration targetConfig = target.GetComponent<PlayerController>().playerConfig;
                        targetConfig.TakeDamage(damage);
                        target.GetComponent<PlayerHealthController>().TakeDamage(damage, criticalHit);
                        originatedFrom.AddShotOnTarget(damage, criticalHit);
                        float targetHealth = target.GetComponent<PlayerHealthController>().health;
                        if (targetHealth <= 0) {
                            originatedFrom.AddKill();
                            targetConfig.AddDeath();
                            logKillForOrigin();
                        }
                        targetConfig.playerHud.GetComponent<PlayerHUDController>().TakeDamage(damage);
                    }
                    
                }
                GameObject effect = Instantiate(primaryWeaponHit, transform.position, Quaternion.identity);
                Destroy(effect, 1);
                DestroyBeam();
            }
        }
        transform.position += transform.up * speed * Time.deltaTime;
    }

    void DestroyBeam() {
        Destroy(gameObject);
    }

    void logKillForOrigin() {
        originatedFrom.playerHud.GetComponent<PlayerHUDController>().AddKill();
    }
}
