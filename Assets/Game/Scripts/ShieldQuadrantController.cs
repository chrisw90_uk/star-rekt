﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldQuadrantController : MonoBehaviour
{
    public int health = 0;
    public int currentHealth;
    private Animator hitAnimation;

    // Start is called before the first frame update
    void Start()
    {
        hitAnimation = GetComponent<Animator>();
        hitAnimation.enabled = false;
    }

    public void TakeDamage(int damage) {
        currentHealth -= damage;
        hitAnimation.enabled = true;
        hitAnimation.Play("Hit");
        hitAnimation.Rebind(); 
    }

    public void Destroy() {
        hitAnimation.enabled = true;
        hitAnimation.Play("Hit");
        hitAnimation.Rebind();
        Invoke("Disable", 0.4f);
    }

    public void RestoreHealth(int value = 0) {
        if (health == 0 && value != 0) {
            health = value;
        }
        if(!gameObject.activeSelf){
            gameObject.SetActive(true);
            hitAnimation.enabled = false;
        }
        currentHealth = health;
        Debug.Log(currentHealth);
    }

    private void Disable() {
        currentHealth = 0;
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (currentHealth <= 0) {
            gameObject.SetActive(false);
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag == "Projectile") {
            hitAnimation.enabled = true;
            hitAnimation.Play("Hit");
            hitAnimation.Rebind();
        }
    }
}
