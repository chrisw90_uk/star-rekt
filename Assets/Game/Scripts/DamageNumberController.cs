﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageNumberController : MonoBehaviour
{
    // Start is called before the first frame update
    private Vector3 positionVariation = new Vector3(1f, 0, 0);
    void Start()
    {
        Destroy(gameObject, 1f);
        transform.localPosition += new Vector3(Random.Range(-positionVariation.x, positionVariation.x), 0, 0);
    }
}
