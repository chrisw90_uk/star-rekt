using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    SpriteRenderer colorChanger;
    Collider2D col;
    private Rigidbody2D rb;

    private void Start() {
		colorChanger = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
	}

    public void Highlight(bool enable) {
        if (enable) {
            colorChanger.color = new Color(153f / 255.0f, 255f / 255.0f, 0f);
        } else {
            colorChanger.color = new Color(255.0f / 255.0f, 255f / 255.0f, 255f / 255.0f);
        }
    }
    public void DisablePhysics() {
        rb.isKinematic = true;
        rb.velocity = new Vector2(0, 0);
        GetComponent<PolygonCollider2D>().enabled = false;
    }
    public void EnablePhysics(float speed) {
        rb.isKinematic = false;
        GetComponent<PolygonCollider2D>().enabled = true;
        rb.AddRelativeForce(Vector3.up * speed * rb.mass, ForceMode2D.Impulse);
    }
}
