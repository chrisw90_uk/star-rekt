﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour
{
    public float mass = 100;
    public float rotate;
    public float rotateIncrement;
    public float speed;
    public float vertical;
    public float horizontal;
    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        //rotate = Random.Range(0f, 360f);
        //rotateIncrement = Random.Range(-0.2f, 0.2f);
        speed = Random.Range(0.5f, 2f);
        vertical = Random.Range(-1f, 1f);
        rb.velocity = new Vector2(-speed, vertical);
        rb.mass = mass * transform.localScale.x; 
    }

    void Update()
    {
        //transform.localRotation = Quaternion.Euler(0, 180, rotate += rotateIncrement);
    }

    void OnBecameInvisible() {
        Destroy(gameObject);
    }
}
