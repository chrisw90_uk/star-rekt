using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerWeaponController : MonoBehaviour {

    public GameObject primaryWeaponBeam;
    public GameObject secondaryWeaponBeam;
    public Transform primaryWeaponOrigin;
    public GameObject weaponOriginEffect;
    public float criticalHitChance;

    // HUD
    public GameObject HUD;

    // Charges and cooldown
    public int primaryCharges;
    private int primaryChargeCurrent;
    public int secondaryCharges;
    private int secondaryChargeCurrent;

    // Projectiles
    public LayerMask canHold;
    public bool isHoldingObject;
    public bool objectIsLocked;
    public Collider2D projectileToHold;
    public GameObject tractorBeam;
    public Transform tractorBeamPosition;
    private bool isCoilingProjectile;
    private float projectileHoldStartTime;
    private Vector2 projectileLockPosition;

    private PlayerControls controls;
    public PlayerConfiguration playerConfig;

    void Start() {
        controls = new PlayerControls();
        isHoldingObject = false;
        objectIsLocked = false;
        //startTime = 0f;
        projectileToHold = null;
        isCoilingProjectile = false;
    }

    public void Init(PlayerConfiguration config)
    {
        playerConfig = config;
        playerConfig.playerInput.onActionTriggered += OnActionTriggered;
        primaryCharges = playerConfig.primaryCharges;
        primaryChargeCurrent = primaryCharges;
        secondaryCharges = playerConfig.secondaryCharges;
        secondaryChargeCurrent = secondaryCharges;
        playerConfig.playerHud.GetComponent<PlayerHUDController>().SetPrimaryCharge(primaryChargeCurrent);
        playerConfig.playerHud.GetComponent<PlayerHUDController>().SetSecondaryCharge(secondaryChargeCurrent);
        playerConfig.playerHud.GetComponent<PlayerHUDController>().SetPrimaryCooldown(playerConfig.primaryCooldown);
        playerConfig.playerHud.GetComponent<PlayerHUDController>().SetSecondaryCooldown(playerConfig.secondaryCooldown);
        criticalHitChance = playerConfig.critHitChance;
    }

    private void OnActionTriggered(InputAction.CallbackContext ctx) {
        if (ctx.action != null) {
            if (ctx.action.name == controls.Player.PrimaryFire.name) {
                OnPrimaryFire(ctx);
            }
            if (ctx.action.name == controls.Player.SecondaryFire.name) {
                OnSecondaryFire(ctx);
            }
            if (ctx.action.name == controls.Player.ToggleProjectile.name) {
                OnToggleProjectile(ctx);
            }
            if (ctx.action.name == controls.Player.LaunchProjectile.name && isHoldingObject) {
                if (ctx.performed) {
                    isCoilingProjectile = true;
                    projectileHoldStartTime = Time.time;
                } else if (ctx.canceled) {
                    float playerSpeed = GetComponent<PlayerController>().speed;
                    if (isCoilingProjectile) {
                        isCoilingProjectile = false;
                        float holdTime = Time.time - projectileHoldStartTime;
                        DropProjectile(playerSpeed + (holdTime * 7f));
                    } else if (isHoldingObject) {
                        DropProjectile(playerSpeed);
                    }
                }
            }
        }
    }

    public void OnPrimaryFire(InputAction.CallbackContext ctx) {
        if(ctx.performed && !isHoldingObject && primaryChargeCurrent > 0) {
            FirePrimaryWeapon();
        }
    }

    public void OnSecondaryFire(InputAction.CallbackContext ctx) {
        if(ctx.performed && !isHoldingObject && secondaryChargeCurrent > 0) {
            FireSecondaryWeapon();
        }
    }

    public void OnToggleProjectile(InputAction.CallbackContext ctx) {
        if(ctx.performed) {
            if (isHoldingObject) {
                float speed = GetComponent<PlayerController>().speed;
                DropProjectile(speed);
            } else if (projectileToHold != null) {
                PickupProjectile();
            }
        }
    }

    void Update() {
        if (!isHoldingObject) {
            DetectProjectile();
        } else {
            float step =  10f * Time.deltaTime; // calculate distance to move
            if (Vector2.Distance(projectileToHold.transform.position, tractorBeamPosition.position) > 0.2f && !objectIsLocked) {
                projectileToHold.transform.position = Vector2.MoveTowards(projectileToHold.transform.position, tractorBeamPosition.position, step);
            } else if (!objectIsLocked){
                projectileToHold.transform.parent = transform;
                objectIsLocked = true;
                projectileLockPosition = projectileToHold.transform.localPosition;
            }
        }
        if (isCoilingProjectile) {
            float holdTime = Time.time - projectileHoldStartTime;
            float shakeSpeed = holdTime * 12;
            float shake = Mathf.Sin(holdTime * shakeSpeed) * 0.3f;
            projectileToHold.transform.localPosition = new Vector2(projectileLockPosition.x + shake, projectileToHold.transform.localPosition.y - 0.001f);
            if (holdTime >= 3) {
                float playerSpeed = GetComponent<PlayerController>().speed;
                DropProjectile(playerSpeed + (holdTime * 7f));
                isCoilingProjectile = false;
            }
        }
    }

    private void FirePrimaryWeapon()
    {
        GameAudioController.Instance.PlayPrimaryWeaponFire();
        primaryChargeCurrent--;
        CreateWeaponBlast(Color.green);
        playerConfig.AddShotFired();
        playerConfig.playerHud.GetComponent<PlayerHUDController>().SetPrimaryCharge(primaryChargeCurrent);
        playerConfig.playerHud.GetComponent<PlayerHUDController>().StartPrimaryCooldown();
        GameObject Beam = Instantiate(primaryWeaponBeam, primaryWeaponOrigin.position, transform.rotation);
        Beam.GetComponent<PrimaryWeaponController>().criticalHitChance = criticalHitChance;
        Beam.GetComponent<PrimaryWeaponController>().originatedFrom = playerConfig;
    }

    public void AddPrimaryCharge() {
        primaryChargeCurrent++;
        playerConfig.playerHud.GetComponent<PlayerHUDController>().SetPrimaryCharge(primaryChargeCurrent);
        if (primaryChargeCurrent == primaryCharges) {
            playerConfig.playerHud.GetComponent<PlayerHUDController>().isOnPrimaryCooldown = false;
        }
    }

    private void FireSecondaryWeapon()
    {
        secondaryChargeCurrent--;
        CreateWeaponBlast(Color.magenta);
        playerConfig.playerHud.GetComponent<PlayerHUDController>().SetSecondaryCharge(secondaryChargeCurrent);
        playerConfig.playerHud.GetComponent<PlayerHUDController>().StartSecondaryCooldown();
        GameObject Beam = Instantiate(secondaryWeaponBeam, primaryWeaponOrigin.position, transform.rotation);
        //Beam.GetComponent<PrimaryWeaponController>().criticalHitChance = criticalHitChance;
        Beam.GetComponent<SecondaryWeaponController>().originatedFrom = playerConfig.playerInput.playerIndex;
    }

    public void AddSecondaryCharge() {
        secondaryChargeCurrent++;
        playerConfig.playerHud.GetComponent<PlayerHUDController>().SetSecondaryCharge(secondaryChargeCurrent);
        if (secondaryChargeCurrent == secondaryCharges) {
            playerConfig.playerHud.GetComponent<PlayerHUDController>().isOnSecondaryCooldown = false;
        }
    }

    private void CreateWeaponBlast(Color color) {
        GameObject Blast = Instantiate(weaponOriginEffect, primaryWeaponOrigin.position, transform.rotation);
        var col = Blast.GetComponent<ParticleSystem>().colorOverLifetime;
        col.color = SetWeaponBlastColor(color);
        Blast.transform.parent = transform;
        Blast.transform.localScale = new Vector3(1, 1, 0);
        Destroy(Blast, 0.5f);
    }

    private Gradient SetWeaponBlastColor(Color color) {
        Gradient grad = new Gradient();
        grad.SetKeys(
            new GradientColorKey[] {
                new GradientColorKey(color, 0.0f),
                new GradientColorKey(color, 1.0f)
            },
            new GradientAlphaKey[] {
                new GradientAlphaKey(1.0f, 0.0f),
                new GradientAlphaKey(0.0f, 1.0f)
            } 
        );
        return grad;
    }

    private void PickupProjectile() {
        tractorBeam.GetComponent<ParticleSystem>().Play();
        projectileToHold.GetComponent<ProjectileController>().DisablePhysics();
        isHoldingObject = true;
    }

    private void DetectProjectile()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, transform.up, 3f, canHold);
        if (hitInfo.collider != null) {
            if (hitInfo.collider.CompareTag("Projectile")) {
                if (projectileToHold != null && (hitInfo.collider.gameObject.GetInstanceID() != projectileToHold.gameObject.GetInstanceID())) {
                    projectileToHold.GetComponent<ProjectileController>().Highlight(false);
                    projectileToHold = null;
                }
                projectileToHold = hitInfo.collider;
                projectileToHold.GetComponent<ProjectileController>().Highlight(true);
            }
        } else {
            if (projectileToHold != null) {
                projectileToHold.GetComponent<ProjectileController>().Highlight(false);
                projectileToHold = null;
            }
        }
    }

    private void DropProjectile(float speed) {
        tractorBeam.GetComponent<ParticleSystem>().Stop();
        projectileToHold.GetComponent<ProjectileController>().EnablePhysics(speed);
        isHoldingObject = false;
        objectIsLocked = false;
        projectileToHold.transform.parent = null;
        projectileToHold = null;
    }
}
