using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHUDController : MonoBehaviour
{

    public int numberOfPlayers;
    private Vector3[] HUDPositions;
    private Vector2[] HUDAnchors;
    public GameObject playerHUD;
    private float hudWidth;
    private float hudHeight;
    private float offset = 0.01f;
    
    // Start is called before the first frame update
    void Awake()
    {
        numberOfPlayers = 0;
        hudWidth = playerHUD.GetComponent<RectTransform>().sizeDelta.x;
        hudHeight = playerHUD.GetComponent<RectTransform>().sizeDelta.y;
        HUDPositions = new Vector3[] {
            new Vector3(0, -hudHeight, 0),
            new Vector3(-hudWidth, -hudHeight, 0),
            new Vector3(0, 0, 0),
            new Vector3(-hudWidth, 0, 0),
        };
        HUDAnchors = new Vector2[] {
            new Vector2(0 + offset, 1 - offset),
            new Vector2(1 - offset, 1 - offset),
            new Vector2(0 + offset, 0 + offset),
            new Vector2(1 - offset, 0 + offset),
        };
    }

    public GameObject CreateHUDForPlayer(PlayerConfiguration playerConfig) {
        GameObject hud = Instantiate(playerHUD, HUDPositions[numberOfPlayers], Quaternion.identity);
        hud.transform.SetParent(gameObject.transform, false);
        hud.transform.SetSiblingIndex(playerConfig.playerIndex);
        RectTransform hudTransform = hud.GetComponent<RectTransform>();
        hudTransform.anchorMin = HUDAnchors[numberOfPlayers];
        hudTransform.anchorMax = HUDAnchors[numberOfPlayers];
        hud.GetComponent<PlayerHUDController>().Init(playerConfig);
        var quadrants = hud.GetComponentsInChildren<PlayerHUDShieldQuadrantController>(true);
            //Loop through quadrants and set shield health
            foreach (var quadrant in quadrants)
                quadrant.RestoreHealth(Convert.ToInt32(playerConfig.shieldStrength));
        numberOfPlayers++;
        return hud;
    }
}
