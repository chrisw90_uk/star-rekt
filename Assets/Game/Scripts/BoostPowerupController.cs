﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostPowerupController : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D col) {
    	if(col.tag == "Player Object"){
			col.gameObject.GetComponent<PlayerController>().FillBoost();
        	Destroy(gameObject);
    	}
    }
}
