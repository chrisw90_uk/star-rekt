using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
	public Rigidbody2D body;
    public GameObject gameHUD;
    public string playerName;
    public float speed;
    public float speedLimit = 2f;
    public float speedIncrement;
    public float remainingBoost;
    public bool isBoosting;
    public bool isCrippled = false;
    public float boostModifier;
    //private float stoppingDistance;

    private PlayerControls controls;
    public PlayerConfiguration playerConfig;
    private bool isUsingController;
    public bool isAccelerating;
    private Vector2 lastStickPosition;
    private Vector2 currentStickPosition;
    private Vector3 mouseWorldPosition;
    private Vector2 mouseScreenPosition;
    private SpriteRenderer spriteRenderer;
    public Vector3 direction;

    // Camera bounds
    float leftBorder;
    float rightBorder;
    float topBorder;
    float bottomBorder;

    void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        controls = new PlayerControls();
        gameHUD = GameObject.Find("GameHUDCanvas");
        spriteRenderer = transform.Find("PlayerSprite").GetComponent<SpriteRenderer>();
        var dist = (transform.position - Camera.main.transform.position).z;
        leftBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x;
        rightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x;
        topBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).y;
        bottomBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, dist)).y;
    }

    // Input system

    private void InitSettings() {
        speed = 0f;
        FillBoost();
        isBoosting = false;
        speedIncrement = 0.04f;
        boostModifier = 2;
    }

    public void Init(PlayerConfiguration config)
    {
        playerConfig = config;
        spriteRenderer.sprite = playerConfig.playerSprite; 
        InitSettings();
        playerConfig.playerInput.onActionTriggered += OnActionTriggered;
    }

    public void Respawn()
    {
        transform.position = playerConfig.playerSpawn.transform.position;
        transform.rotation = playerConfig.playerSpawn.transform.rotation;
        InitSettings();
    }

    private void OnActionTriggered(InputAction.CallbackContext ctx) {
        if (ctx.action.name == controls.Player.Go.name) {
            OnKeyboardMove(ctx);
        }
        if (ctx.action.name == controls.Player.Boost.name) {
            OnBoost(ctx);
        }
        if (ctx.action.name == controls.Player.Movement.name) {
            OnControllerMove(ctx);
        }
        if (ctx.action.name == controls.Player.MousePosition.name) {
            OnMouseMove(ctx);
        }
        if (ctx.action.name == controls.Player.Movement.name) {
            OnControllerMove(ctx);
        }
    }
    
    public void OnKeyboardMove(InputAction.CallbackContext ctx) {
        ctx.action.performed += _ => isAccelerating = true;
        ctx.action.canceled += _ => isAccelerating = false;
    }

    public void OnControllerMove(InputAction.CallbackContext ctx) {
        ctx.action.performed += _ => {
            isUsingController = true;
            currentStickPosition = ctx.ReadValue<Vector2>();
            if (float.IsNaN(currentStickPosition.x)) {
                isAccelerating = false;
            } else {
                lastStickPosition = currentStickPosition;
                isAccelerating = true;
            }
        };
        ctx.action.canceled += _ => {
            isAccelerating = false;
        }; 
    }

    public void OnBoost(InputAction.CallbackContext ctx) {
        ctx.action.performed += _ => {
            if (!isCrippled && remainingBoost > 0f) {
                isBoosting = true;
                SetSpeedLimit(12);
            }
        };
        ctx.action.canceled += _ => {
            isBoosting = false;
            if (isCrippled) {
                SetSpeedLimit(2);
            } else {
                SetSpeedLimit(5);
            } 
        };
    }

    public void OnMouseMove(InputAction.CallbackContext ctx) {
        var value = ctx.ReadValue<Vector2>();
        if (value != Vector2.zero) {
            mouseScreenPosition = value;
        }
    }

    void Update()
    {
        bool isDyingAndRespawning = GetComponent<PlayerHealthController>().isDyingAndRespawning;

        if (!isDyingAndRespawning)
            RotatePlayer();

        if(isBoosting){
            if (remainingBoost > 0) {
                SetSpeedLimit(12);
                if(speed > 0 && !isCrippled) {
                    remainingBoost -= 0.05f;
                    playerConfig.playerHud.GetComponent<PlayerHUDController>().SetBoostAmount(remainingBoost);
                }
            } else if (speedLimit != 5) {
                SetSpeedLimit(5f);
            }
        }
        float distanceToMouse = Vector2.Distance(new Vector2(mouseWorldPosition.x, mouseWorldPosition.y), new Vector2(transform.position.x, transform.position.y));
        if (distanceToMouse > 0.5f) {
            if (isAccelerating && !isDyingAndRespawning) {
                Accelerate();
            } else {
                Decelerate();
            }
        } else if (speed > 0) {
            FullStop();
        }
    }

    void FixedUpdate() {
        body.MovePosition(transform.position + transform.up * Time.deltaTime * speed);
        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, leftBorder, rightBorder),
            Mathf.Clamp(transform.position.y, topBorder, bottomBorder),
            transform.position.z
        );
    }

    private void RotatePlayer() {
        float angle;
        Quaternion rotation;
        if (isUsingController) {
            angle = Mathf.Atan2(lastStickPosition.y, lastStickPosition.x) * Mathf.Rad2Deg - 90f;
            Quaternion rotateTo = Quaternion.Euler(new Vector3(0, 0, angle));
            rotation = Quaternion.Lerp(transform.rotation, rotateTo, Time.deltaTime * 10f);
        } else {
            mouseWorldPosition = Camera.main.ScreenToWorldPoint(mouseScreenPosition);
            direction = mouseWorldPosition - transform.position;
            angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
            rotation = Quaternion.Euler(new Vector3(0f, 0f, angle));
        }
        transform.rotation = rotation;
        var weaponController = GetComponent<PlayerWeaponController>();
        if (weaponController.isHoldingObject) {
            weaponController.projectileToHold.transform.rotation = rotation;
        }
    }

    private void Accelerate()
    {
        if (speed <= speedLimit)
        {
            if (isBoosting){
                speed += (speedIncrement * boostModifier);
            } else {
                speed += speedIncrement;
            }
            if (speed >= speedLimit) {
                speed = speedLimit;
            }
        } else {
            // Speed correction after boost
            speed -= speedIncrement;
        }
    }

    private void Decelerate()
    {
        speed -= speedIncrement;
        if (speed <= 0) {
            speed = 0;
        }
    }

    private void FullStop()
    {
        speed -= 0.1f;
        if (speed <= 0) {
            speed = 0;
        }
    }

    private void SetSpeedLimit(float limit) {
        speedLimit = limit;
    }

    public void Cripple(){
        transform.Find("PlayerCripple").GetComponent<ParticleSystem>().Play();
        isBoosting = false;
        isCrippled = true;
        SetSpeedLimit(2);
        StartCoroutine(FadeSprite(0.6f, 0.6f, 0.6f, 5f));
        Invoke("Uncripple", 5f);
    }

    public void Uncripple(){
        isCrippled = false;
        transform.Find("PlayerCripple").GetComponent<ParticleSystem>().Stop();
        SetSpeedLimit(5f);
        StartCoroutine(FadeSprite(1f, 1f, 1f, 5f));
    }

    public void FillBoost() {
        remainingBoost = playerConfig.boost;
    }

    IEnumerator FadeSprite(float r, float g, float b, float time) {
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / time) {
            float currentR = spriteRenderer.color.r;
            float currentG = spriteRenderer.color.g;
            float currentB = spriteRenderer.color.b;
            spriteRenderer.color = new Color(
                Mathf.Lerp(currentR, r, t),
                Mathf.Lerp(currentG, g, t),
                Mathf.Lerp(currentB, b, t),
                1f
            );
            yield return null;
        }
    }
}
