﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondaryWeaponController : MonoBehaviour
{

    public float speed;
    public LayerMask canHit;
    public int originatedFrom;
    public GameObject impact;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroyBeam", 3f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.up * speed * Time.deltaTime;
        transform.Rotate(Vector3.up, 10f * Time.deltaTime);
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, transform.up, 0.1f, canHit);
        if (hitInfo.collider != null) {
            PlayerConfiguration playerConfig = hitInfo.collider.transform.GetComponentInParent<PlayerController>().playerConfig;
            var playerIndex = playerConfig.playerInput.playerIndex;
            if (playerIndex != originatedFrom) {
                if (hitInfo.collider.CompareTag("Shield")) {
                    hitInfo.collider.GetComponent<ShieldQuadrantController>().Destroy();
                    playerConfig.playerHud.GetComponent<PlayerHUDController>().DestroyShield(hitInfo.collider.name);
                }
                if (hitInfo.collider.CompareTag("Player")) {
                    var target = hitInfo.collider.transform.parent;
                    target.GetComponent<PlayerController>().Cripple();
                }
                DestroyBeam();
                GameObject effect = Instantiate(impact, transform.position, Quaternion.identity);
                Destroy(effect, 1);    
            }  
        }
    }
    void DestroyBeam() {
        Destroy(gameObject);
    }
}
