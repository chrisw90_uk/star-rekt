using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthController : MonoBehaviour {

    private PlayerConfiguration playerConfig;
    private int playerIndex;
    // Health
    public float health;

    // Damage
    public GameObject damageNumber;

    // Death
    public GameObject playerExplosion;
    GameObject explosionTakingPlace;
    public bool isDyingAndRespawning;

    // Shields
    private ShieldQuadrantController[] quadrants;

    void Start() {
        playerConfig = GetComponent<PlayerController>().playerConfig;
        playerIndex = playerConfig.playerInput.playerIndex;
        quadrants = gameObject.GetComponentsInChildren<ShieldQuadrantController>(true);
        RestoreHealth();
        SetMaxShieldStrength();
    }

    public void RestoreHealth() {
        health = playerConfig.health;
    }

    public void SetMaxShieldStrength() {
        foreach (var quadrant in quadrants)
            quadrant.RestoreHealth(Convert.ToInt32(playerConfig.shieldStrength));
    }

    public void TakeDamage(float damage, bool criticalHit) {
        health -= damage;
        var damageText = Instantiate(damageNumber, transform.position, Quaternion.identity, transform);
        damageText.transform.GetChild(0).GetComponent<MeshRenderer>().sortingLayerName = "UI";
        var textMesh = damageText.transform.GetChild(0).GetComponent<TextMesh>();
        if (criticalHit) {
            textMesh.fontSize = 55;
            textMesh.color = new Color(1f, 0, 0, 1f);
        }
        textMesh.text = damage.ToString();
        if (health <= 0) {
            Die();
        }
    }

    private void Die() {
        isDyingAndRespawning = true;
        transform.Find("PlayerShields").transform.localScale = new Vector3(0, 0, 0);
        transform.Find("PlayerSprite").GetComponent<PolygonCollider2D>().enabled = false;
        playerConfig.playerHud.GetComponent<PlayerHUDController>().AddDeath();
        var particles = GetComponentsInChildren<ParticleSystem>();
        foreach (var system in particles)
        {
            system.Stop();
        }
        if(explosionTakingPlace == null) {
            explosionTakingPlace = Instantiate(playerExplosion, transform.position, Quaternion.identity);
            explosionTakingPlace.transform.parent = transform;
            Destroy(explosionTakingPlace, 3);  
        }
        Invoke("HidePlayer", 0.3f);
        Invoke("Respawn", 3f);
    }

    private void HidePlayer() {
        transform.Find("PlayerSprite").transform.localScale = new Vector3(0, 0, 0);
    }

    private void Respawn() {
        RestoreHealth();
        playerConfig.playerHud.GetComponent<PlayerHUDController>().Spawn();
        GetComponent<PlayerController>().Respawn();
        transform.Find("PlayerShields").transform.localScale = new Vector3(1.8f, 1.8f, 1);
        transform.Find("PlayerSprite").transform.localScale = new Vector3(1f, 1f, 1);
        transform.Find("PlayerSprite").GetComponent<PolygonCollider2D>().enabled = true;
        var engine = transform.Find("PlayerSprite").transform.Find("PlayerEngine").GetComponent<ParticleSystem>();
        engine.Play();
        //Loop through quadrants and restore health
        foreach (var quadrant in quadrants)
            quadrant.RestoreHealth();
        isDyingAndRespawning = false;
    }
}
