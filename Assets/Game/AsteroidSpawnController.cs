using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawnController : MonoBehaviour
{
    public Sprite[] asteroidSprites;
    public GameObject toSpawn;

    void Start() {
        InvokeRepeating("Spawn", 0f, 1.5f);
    }       

    public void Spawn () 
    {
        float distanceFromCamera = Camera.main.nearClipPlane;
        Vector3 topRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, distanceFromCamera));
        Vector3 bottomRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distanceFromCamera));
        float y = Random.Range(topRight.y, bottomRight.y);
        float size = Random.Range(0.05f, 0.25f);
        var asteroid = Instantiate(toSpawn, new Vector3(topRight.x + 0.1f, y, 0), Quaternion.identity);
        asteroid.transform.parent = transform;
        asteroid.transform.localScale = new Vector3(size, size, 0);
        int index = Random.Range(0, asteroidSprites.Length - 1);
        asteroid.GetComponent<SpriteRenderer>().sprite = asteroidSprites[index];
        asteroid.AddComponent<PolygonCollider2D>();
        if (size < 0.15f) {
            asteroid.GetComponent<ParticleSystem>().Play();
        }
    }
}
