// GENERATED AUTOMATICALLY FROM 'Assets/PlayerControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerControls"",
    ""maps"": [
        {
            ""name"": ""Player"",
            ""id"": ""3a1eabf2-44ab-4395-aa5a-a2e6e09329f5"",
            ""actions"": [
                {
                    ""name"": ""Go"",
                    ""type"": ""Button"",
                    ""id"": ""8b82c4cc-b9b9-4a73-a280-8b6862d3f34f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PrimaryFire"",
                    ""type"": ""Value"",
                    ""id"": ""60e4aade-b42d-40f2-ae84-d01a3dd4ea6a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MousePosition"",
                    ""type"": ""PassThrough"",
                    ""id"": ""216dd288-b161-4035-96fe-65b85539fb1f"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ToggleProjectile"",
                    ""type"": ""Button"",
                    ""id"": ""0b5a273c-cc55-4fb0-aaea-4e67d769f140"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""LaunchProjectile"",
                    ""type"": ""Button"",
                    ""id"": ""0de4eee0-9c8f-4914-84d9-360f26fc0204"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Boost"",
                    ""type"": ""Button"",
                    ""id"": ""0e575fae-fc8d-489a-8fb9-b28277d3a51b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Movement"",
                    ""type"": ""Value"",
                    ""id"": ""d7107e75-feb8-4d81-b396-b36f6e4b16c3"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SecondaryFire"",
                    ""type"": ""Button"",
                    ""id"": ""503bd3bc-858d-4991-b77a-322f3c73bcc0"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MenuSelect"",
                    ""type"": ""PassThrough"",
                    ""id"": ""a806c2f7-7ea7-46ba-a4e0-c5c07150a522"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MenuNavigation"",
                    ""type"": ""PassThrough"",
                    ""id"": ""7dd75c82-0b21-4187-9052-1736de7c3f0d"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MenuMousePosition"",
                    ""type"": ""PassThrough"",
                    ""id"": ""65df63e2-b1d6-450a-8769-1ac7060f7c71"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MenuCancel"",
                    ""type"": ""PassThrough"",
                    ""id"": ""93f2a8a3-a76c-417e-8f11-3af4c26550c8"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MenuClick"",
                    ""type"": ""PassThrough"",
                    ""id"": ""20abc111-5a23-47dc-a3a4-117c102e9827"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""9e09c059-313a-4b59-8f4d-7bb1109c2942"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""Go"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7072525f-037a-43b7-bf8d-46a28301cd5f"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": ""Press(pressPoint=1)"",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""PrimaryFire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""542ad520-c82a-43b9-a9d4-cc79ac71d0b2"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": ""Press(pressPoint=1)"",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""PrimaryFire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a11b6925-0260-4d8f-af55-d7a09fee8c63"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MousePosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""31500e52-6471-4944-92c8-3831b93193e7"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""ToggleProjectile"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4ddb3a72-1fbd-44a5-bd43-ea03e1b16c24"",
                    ""path"": ""<Gamepad>/buttonWest"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""ToggleProjectile"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""bafdb020-b61c-4551-8ef4-450514006097"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": ""Hold"",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""LaunchProjectile"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""47e8af40-45a7-44fc-bbb5-15485d5a9dc8"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""LaunchProjectile"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2f68296c-aaa5-414b-a87b-9b73f67f3def"",
                    ""path"": ""<Keyboard>/shift"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""Boost"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""25dd9cd6-ad53-4cb9-b799-df73cfdddcc7"",
                    ""path"": ""<Gamepad>/rightTrigger"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Boost"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a8f53840-ea48-4ae0-8f63-b9ab227deb7f"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""10c51184-9dca-4d30-a85a-3dcc3329288e"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""SecondaryFire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""775bf791-ed4b-4831-bc11-968a9601ee79"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""SecondaryFire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""cd9e2e14-787e-4c22-838b-45fd711c5c85"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""MenuSelect"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a612d469-9833-4216-86c6-7606ada874c4"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""MenuSelect"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""2eb276ca-d2e6-48dc-b9f4-7a32c1c9e681"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""MenuNavigation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""2D Vector"",
                    ""id"": ""331ee0da-db2e-4407-bd1b-c12fbfeae3d1"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MenuNavigation"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""bffb84c1-d804-4196-bb69-4eccfd092869"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""MenuNavigation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""8a34e65f-6505-4bc3-901e-714f87d98679"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""MenuNavigation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""8a8bcf94-e770-4eb3-aad2-1aaf38656f44"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""MenuNavigation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""a2014e4d-6425-430c-b39e-4a362c044a4b"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MenuNavigation"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""8cf3ae1e-3896-48d2-8438-b45f35bfb08a"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""MenuMousePosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0cf61c52-8832-4c53-8058-8e21699d3678"",
                    ""path"": ""<Gamepad>/buttonEast"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""MenuCancel"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""eb2ae37c-143c-4e0c-90ae-af67dc2a50e5"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard and Mouse"",
                    ""action"": ""MenuClick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Controller"",
            ""bindingGroup"": ""Controller"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Keyboard and Mouse"",
            ""bindingGroup"": ""Keyboard and Mouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Player
        m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
        m_Player_Go = m_Player.FindAction("Go", throwIfNotFound: true);
        m_Player_PrimaryFire = m_Player.FindAction("PrimaryFire", throwIfNotFound: true);
        m_Player_MousePosition = m_Player.FindAction("MousePosition", throwIfNotFound: true);
        m_Player_ToggleProjectile = m_Player.FindAction("ToggleProjectile", throwIfNotFound: true);
        m_Player_LaunchProjectile = m_Player.FindAction("LaunchProjectile", throwIfNotFound: true);
        m_Player_Boost = m_Player.FindAction("Boost", throwIfNotFound: true);
        m_Player_Movement = m_Player.FindAction("Movement", throwIfNotFound: true);
        m_Player_SecondaryFire = m_Player.FindAction("SecondaryFire", throwIfNotFound: true);
        m_Player_MenuSelect = m_Player.FindAction("MenuSelect", throwIfNotFound: true);
        m_Player_MenuNavigation = m_Player.FindAction("MenuNavigation", throwIfNotFound: true);
        m_Player_MenuMousePosition = m_Player.FindAction("MenuMousePosition", throwIfNotFound: true);
        m_Player_MenuCancel = m_Player.FindAction("MenuCancel", throwIfNotFound: true);
        m_Player_MenuClick = m_Player.FindAction("MenuClick", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player
    private readonly InputActionMap m_Player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private readonly InputAction m_Player_Go;
    private readonly InputAction m_Player_PrimaryFire;
    private readonly InputAction m_Player_MousePosition;
    private readonly InputAction m_Player_ToggleProjectile;
    private readonly InputAction m_Player_LaunchProjectile;
    private readonly InputAction m_Player_Boost;
    private readonly InputAction m_Player_Movement;
    private readonly InputAction m_Player_SecondaryFire;
    private readonly InputAction m_Player_MenuSelect;
    private readonly InputAction m_Player_MenuNavigation;
    private readonly InputAction m_Player_MenuMousePosition;
    private readonly InputAction m_Player_MenuCancel;
    private readonly InputAction m_Player_MenuClick;
    public struct PlayerActions
    {
        private @PlayerControls m_Wrapper;
        public PlayerActions(@PlayerControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Go => m_Wrapper.m_Player_Go;
        public InputAction @PrimaryFire => m_Wrapper.m_Player_PrimaryFire;
        public InputAction @MousePosition => m_Wrapper.m_Player_MousePosition;
        public InputAction @ToggleProjectile => m_Wrapper.m_Player_ToggleProjectile;
        public InputAction @LaunchProjectile => m_Wrapper.m_Player_LaunchProjectile;
        public InputAction @Boost => m_Wrapper.m_Player_Boost;
        public InputAction @Movement => m_Wrapper.m_Player_Movement;
        public InputAction @SecondaryFire => m_Wrapper.m_Player_SecondaryFire;
        public InputAction @MenuSelect => m_Wrapper.m_Player_MenuSelect;
        public InputAction @MenuNavigation => m_Wrapper.m_Player_MenuNavigation;
        public InputAction @MenuMousePosition => m_Wrapper.m_Player_MenuMousePosition;
        public InputAction @MenuCancel => m_Wrapper.m_Player_MenuCancel;
        public InputAction @MenuClick => m_Wrapper.m_Player_MenuClick;
        public InputActionMap Get() { return m_Wrapper.m_Player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                @Go.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnGo;
                @Go.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnGo;
                @Go.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnGo;
                @PrimaryFire.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPrimaryFire;
                @PrimaryFire.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPrimaryFire;
                @PrimaryFire.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnPrimaryFire;
                @MousePosition.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMousePosition;
                @MousePosition.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMousePosition;
                @MousePosition.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMousePosition;
                @ToggleProjectile.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnToggleProjectile;
                @ToggleProjectile.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnToggleProjectile;
                @ToggleProjectile.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnToggleProjectile;
                @LaunchProjectile.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLaunchProjectile;
                @LaunchProjectile.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLaunchProjectile;
                @LaunchProjectile.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLaunchProjectile;
                @Boost.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnBoost;
                @Boost.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnBoost;
                @Boost.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnBoost;
                @Movement.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMovement;
                @SecondaryFire.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSecondaryFire;
                @SecondaryFire.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSecondaryFire;
                @SecondaryFire.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSecondaryFire;
                @MenuSelect.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuSelect;
                @MenuSelect.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuSelect;
                @MenuSelect.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuSelect;
                @MenuNavigation.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuNavigation;
                @MenuNavigation.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuNavigation;
                @MenuNavigation.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuNavigation;
                @MenuMousePosition.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuMousePosition;
                @MenuMousePosition.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuMousePosition;
                @MenuMousePosition.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuMousePosition;
                @MenuCancel.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuCancel;
                @MenuCancel.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuCancel;
                @MenuCancel.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuCancel;
                @MenuClick.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuClick;
                @MenuClick.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuClick;
                @MenuClick.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMenuClick;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Go.started += instance.OnGo;
                @Go.performed += instance.OnGo;
                @Go.canceled += instance.OnGo;
                @PrimaryFire.started += instance.OnPrimaryFire;
                @PrimaryFire.performed += instance.OnPrimaryFire;
                @PrimaryFire.canceled += instance.OnPrimaryFire;
                @MousePosition.started += instance.OnMousePosition;
                @MousePosition.performed += instance.OnMousePosition;
                @MousePosition.canceled += instance.OnMousePosition;
                @ToggleProjectile.started += instance.OnToggleProjectile;
                @ToggleProjectile.performed += instance.OnToggleProjectile;
                @ToggleProjectile.canceled += instance.OnToggleProjectile;
                @LaunchProjectile.started += instance.OnLaunchProjectile;
                @LaunchProjectile.performed += instance.OnLaunchProjectile;
                @LaunchProjectile.canceled += instance.OnLaunchProjectile;
                @Boost.started += instance.OnBoost;
                @Boost.performed += instance.OnBoost;
                @Boost.canceled += instance.OnBoost;
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
                @SecondaryFire.started += instance.OnSecondaryFire;
                @SecondaryFire.performed += instance.OnSecondaryFire;
                @SecondaryFire.canceled += instance.OnSecondaryFire;
                @MenuSelect.started += instance.OnMenuSelect;
                @MenuSelect.performed += instance.OnMenuSelect;
                @MenuSelect.canceled += instance.OnMenuSelect;
                @MenuNavigation.started += instance.OnMenuNavigation;
                @MenuNavigation.performed += instance.OnMenuNavigation;
                @MenuNavigation.canceled += instance.OnMenuNavigation;
                @MenuMousePosition.started += instance.OnMenuMousePosition;
                @MenuMousePosition.performed += instance.OnMenuMousePosition;
                @MenuMousePosition.canceled += instance.OnMenuMousePosition;
                @MenuCancel.started += instance.OnMenuCancel;
                @MenuCancel.performed += instance.OnMenuCancel;
                @MenuCancel.canceled += instance.OnMenuCancel;
                @MenuClick.started += instance.OnMenuClick;
                @MenuClick.performed += instance.OnMenuClick;
                @MenuClick.canceled += instance.OnMenuClick;
            }
        }
    }
    public PlayerActions @Player => new PlayerActions(this);
    private int m_ControllerSchemeIndex = -1;
    public InputControlScheme ControllerScheme
    {
        get
        {
            if (m_ControllerSchemeIndex == -1) m_ControllerSchemeIndex = asset.FindControlSchemeIndex("Controller");
            return asset.controlSchemes[m_ControllerSchemeIndex];
        }
    }
    private int m_KeyboardandMouseSchemeIndex = -1;
    public InputControlScheme KeyboardandMouseScheme
    {
        get
        {
            if (m_KeyboardandMouseSchemeIndex == -1) m_KeyboardandMouseSchemeIndex = asset.FindControlSchemeIndex("Keyboard and Mouse");
            return asset.controlSchemes[m_KeyboardandMouseSchemeIndex];
        }
    }
    public interface IPlayerActions
    {
        void OnGo(InputAction.CallbackContext context);
        void OnPrimaryFire(InputAction.CallbackContext context);
        void OnMousePosition(InputAction.CallbackContext context);
        void OnToggleProjectile(InputAction.CallbackContext context);
        void OnLaunchProjectile(InputAction.CallbackContext context);
        void OnBoost(InputAction.CallbackContext context);
        void OnMovement(InputAction.CallbackContext context);
        void OnSecondaryFire(InputAction.CallbackContext context);
        void OnMenuSelect(InputAction.CallbackContext context);
        void OnMenuNavigation(InputAction.CallbackContext context);
        void OnMenuMousePosition(InputAction.CallbackContext context);
        void OnMenuCancel(InputAction.CallbackContext context);
        void OnMenuClick(InputAction.CallbackContext context);
    }
}
