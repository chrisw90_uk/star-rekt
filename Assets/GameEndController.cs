﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameEndController : MonoBehaviour
{
    public GameObject[] playerScoreRows;
    private IEnumerable<PlayerConfiguration> playersSortedByKDR;
    private float[] bottomPositions = new float[] { 528, 407, 290 };
    public void SetScores(List<PlayerConfiguration> playerConfigs) {
        playersSortedByKDR = playerConfigs.OrderByDescending(p => p.kdr);
        int numberOfPlayers = playerConfigs.Count;
        SetHeightOfResultsPanel(bottomPositions[numberOfPlayers - 2]);
        for (int i = 0; i < playerScoreRows.Length; i++) {
            UpdateStatsOnRow(i);
        }
    }

    private void SetHeightOfResultsPanel(float bottom) {
        RectTransform rectTransform = GetComponent<RectTransform>();
        rectTransform.offsetMin = new Vector2(rectTransform.offsetMin.x, bottom); // new Vector2(left, bottom);
    }

    private void UpdateStatsOnRow(int i) {
        if (i > (playersSortedByKDR.Count() - 1)) {
            playerScoreRows[i].SetActive(false);
        } else {
            PlayerConfiguration playerConfig = playersSortedByKDR.ElementAt(i);
            GameObject row = playerScoreRows[i];
            //Set Sprite
            Image SpriteImage = row.transform.Find("Sprite").transform.Find("SpriteImage").GetComponent<Image>();
            SpriteImage.sprite = playerConfig.playerSprite;
            //Set Stats
            GetStat(row, "KillsStat").text = playerConfig.kills.ToString("N0");
            GetStat(row, "DeathsStat").text = playerConfig.deaths.ToString("N0");
            GetStat(row, "KDRStat").text = playerConfig.kdr.ToString("0.#");
            GetStat(row, "DamageDealtStat").text = playerConfig.damageDealt.ToString("N0");
            GetStat(row, "DamageTakenStat").text = playerConfig.damageTaken.ToString("N0");
            GetStat(row, "CritHitStat").text = playerConfig.criticalHits.ToString("N0");
            GetStat(row, "CritHitDmgStat").text = playerConfig.criticalHitDamage.ToString("N0");
            GetStat(row, "AccuracyStat").text = playerConfig.accuracy.ToString("0.#") + "%";
        }
    }

    private TextMeshProUGUI GetStat(GameObject row, string statName) {
        return row.transform.Find("Stats").transform.Find(statName).GetComponent<TextMeshProUGUI>();
    }
}
